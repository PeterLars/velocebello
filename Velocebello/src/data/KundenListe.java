package data;

import java.util.LinkedList;
import java.util.List;

public class KundenListe {
	/**
	 * Bezeichnung der Attribute der Klasse Kunde
	 * stellt für einen JTable die Spaltennamen bereit
	 */
	public static final String[] SPALTENNAMEN = {"Kundennummer", "Vorname",
			"Nachname", "Strasse", "PLZ", "Ort", "Telefonnummer" };
	private List<Kunde> kundenliste;

	public KundenListe() {
		this.kundenliste = new LinkedList<Kunde>();
	}

	public void addKunde(Kunde k){
		this.kundenliste.add(k);
	}
	
	public List<Kunde> getKundenliste() {
		return kundenliste;
	}

	public void setKundenliste(List<Kunde> kundenliste) {
		this.kundenliste = kundenliste;
	}

	/**
	 * stellt für ein JTable die Tabellendaten bereit
	 * @return
	 */
	public String[][] getKunden() {
		String[][] tabledata = new String[kundenliste.size()][SPALTENNAMEN.length];
		for (int i = 0; i < kundenliste.size(); i++) {
			tabledata[i][0] = kundenliste.get(i).getKundennummer() + "";
			tabledata[i][1] = kundenliste.get(i).getVorname();
			tabledata[i][2] = kundenliste.get(i).getNachname();
			tabledata[i][3] = kundenliste.get(i).getStrasse();
			tabledata[i][4] = kundenliste.get(i).getPlz();
			tabledata[i][5] = kundenliste.get(i).getOrt();
			tabledata[i][6] = kundenliste.get(i).getTelefonnummer();
		}
		return tabledata;
	}
}